const app = Vue.createApp({
    data() {
        return {
            newsData: [],
            category: "",
            country: ""
        }
    },
    methods: {
        handleFilters(country, category, search) {
            console.log(country, category, search)
            this.fetchNewsFeed(this.country, this.category, search);
        },
        
        async fetchNewsFeed(country = "ph", category = "general") {
            this.errors = null;
            this.loading = true;
            try {
                const response = await fetch(`https://newsapi.org/v2/top-headlines?country=${country}&category=${category}&apiKey=3490bf7f634147ec9e05945785c89e32`);
                if (!response.ok) {
                    const error = new Error("Failed to fetch Data");
                    error.statusCode = 404;
                    throw error;
                }
                else {
                    const data = await response.json();
                    this.newsData = data.articles;
                    console.log("newsData", this.newsData)
                    this.loading = false;
                }
            } catch (error) {
                console.log(error);
                this.loading = false;
                this.errors = "Something went wrong - try again later!";
            }
        }
    },
    async mounted() {
        await this.fetchNewsFeed()
    },
    computed: {
        theDate() {
            return moment(this.newsData.publishedAt).format('MMM Do YYYY');
        }
    }
})

app.mount('#app')